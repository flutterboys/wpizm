import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'wpMain.dart';
import 'slider.dart';

//initial main loader
/**
void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}
**/

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 7,
      navigateAfterSeconds: new AfterSplash(),
      title: new Text('Welcome to the Jungle!',
        style: new TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20.0
        ),),
      image: new Image.network('https://flutter.io/images/catalog-widget-placeholder.png'),
      backgroundColor: Colors.white,
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 100.0,
      onClick: ()=>print("Flutter Egypt"),
      loaderColor: Colors.red,
    );
  }
}

class AfterSplash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(

        title: new Text("DGC STUDIOS: WP API v1"),
        backgroundColor: Colors.black
        automaticallyImplyLeading: false,
      ),
      body: new VirtuoozaHome()
    );
  }
}

//hold
/**
    new Center(
    child: new Text("Succeeded!",
    style: new TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 30.0
    ),),

    ),


**/