/// test routes

import 'package:flutter/material.dart';
///set the page routes
import 'package:router/screens/home.dart';
import 'package:router/screens/slider.dart';
import 'package:router/screens/wpMain.dart';
import 'package:router/screens/splash.dart';

void main() => runApp (new MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build (BuildContext context){
    return new MaterialApp(
        title: 'navi',
        routes: <string,WidgetBuilder>{
          '/home': (buildcontext context) => new Home(),
          '/home': (buildcontext context) => new slider(),
          '/home': (buildcontext context) => new wpMain(),
          '/home': (buildcontext context) => new splash(),
        }
    );
  }
}
